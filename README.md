## Clone the project

`git clone https://alfcastaneda@bitbucket.org/alfcastaneda/coup.git`

## Install all dependencies

`yarn` or `npm` install (whatever is installed on your system)

## Create a .env file

Create a `.env` file and place there the url to get items from in a varialble called `REACT_APP_API_URL`
in this case I used:

```
REACT_APP_API_URL=https://qc05n0gp78.execute-api.eu-central-1.amazonaws.com/prod
```

after that just.

## Run the project

-   `yarn` or `npm` start
