import * as axios from 'axios';
const ENDPOINT = process.env.REACT_APP_API_URL;

const HttpGET = (endpoint = '', data = {}, options) =>
    axios.get(`${ENDPOINT}/${endpoint}`, { params: data }, options);

export { HttpGET };
