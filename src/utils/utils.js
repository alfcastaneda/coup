const DEFAULT_SPLITER = 5;
/**
 * Get a substring with the passed number of charactes
 *
 * @param {string} str - the original string
 * @param {number} numberOfChar - the desired number of characters of the substring defaults to 5
 * @returns {string}
 */
const getLastCharactersOfString = (str, numberOfChar = DEFAULT_SPLITER) =>
    str.substr(str.length - numberOfChar);

/**
 * Deep clone an array of objects
 *
 * @param {Array} arrayOfObjects
 * @param {Object} options - extra values to add to each entity
 * @returns {Object[]}
 */
const copyArrayofObjects = (arrayOfObjects, options) =>
    arrayOfObjects.map(value => Object.assign({}, value, options));
export { getLastCharactersOfString, copyArrayofObjects };
