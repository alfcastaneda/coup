import React, { Component, Fragment } from 'react';

import './App.css';

import ScootersTable from './scooters/ScootersTable';

class App extends Component {
    render() {
        return (
            <Fragment>
                <header>
                    <h1>List of scooters</h1>
                </header>
                <section className="App">
                    <ScootersTable />
                </section>
            </Fragment>
        );
    }
}

export default App;
