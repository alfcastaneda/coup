import React from 'react';
import ScooterTableCell from './ScooterTableCell';
import { getLastCharactersOfString } from '../utils/utils';
const ScooterTableRow = ({ scooter }) => {
    const getCellComponent = (column, value) => {
        switch (column) {
            case 'energy_level':
                return (
                    <div style={{ color: value < 20 ? 'red' : 'black' }}>{value}%</div>
                );
            case 'distance_to_travel':
                return <div>{value} km</div>;
            case 'id':
            case 'market_id':
                return <div>{getLastCharactersOfString(value)}</div>;
            case 'location':
                return (
                    <div>
                        <button>Location</button>
                        {/* Lat: <a href="#">{value.lat}</a>
                        <br />
                        Lng: <a href="#"> {value.lng}</a> */}
                    </div>
                );
            default:
                return <div>{value}</div>;
        }
    };
    return (
        <tr className="table--row">
            <td />
            {Object.keys(scooter).map(column => {
                return (
                    <ScooterTableCell key={`td-${scooter.id}-${column}`}>
                        {getCellComponent(column, scooter[column])}
                    </ScooterTableCell>
                );
            })}
        </tr>
    );
};
export default ScooterTableRow;
