import React from 'react';
const ScooterTableCell = props => (
    <td>
        {props.value}
        {props.children}
    </td>
);
export default ScooterTableCell;
