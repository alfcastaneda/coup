import React, { Component } from 'react';
import { HttpGET } from '../utils/http';
import Table from '../UI/Table';
import SelectInput from '../UI/SelectInput';
import TextInput from '../UI/TextInput';
import ScooterTableRow from './ScooterTableRow';
import { copyArrayofObjects } from '../utils/utils';

import './scooters.css';

const SCOOTERS_ENDPOINT = 'scooters';
const DEFAULT_FILTERS = {
    _limit: 10,
    _page: 0,
    search: ''
};
const MIN_SEARCH_LENGTH = 3;
const ALL_ITEMS_VALUE = 'All';
const PAGE_RANGES = [10, 20, 30, 40, 50, ALL_ITEMS_VALUE];
const REFRESH_TIME = 10000;
class ScootersTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filters: DEFAULT_FILTERS,
            metaData: {},
            tableHeaders: [''],
            tableData: []
        };
        this.updateParams = this.updateParams.bind(this);
        this.updateSearch = this.updateSearch.bind(this);
    }
    componentWillMount() {
        this.interval = setInterval(() => this.getEntries(), REFRESH_TIME);
        this.getEntries();
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }
    updateParams(event) {
        const _limit = event.target.value;
        this.setState(prevState => {
            const newFilters = Object.assign({}, prevState.filters, {
                _limit
            });
            return { filters: newFilters };
        }, this.updateList);
    }
    updateSearch(value) {
        this.setState(
            prevState => ({
                filters: Object.assign({}, prevState.filters, { search: value })
            }),
            this.updateList
        );
    }
    updateList() {
        this.setState(prevState => {
            let newScootersList = copyArrayofObjects(prevState.scooters).filter(
                obj =>
                    obj.model.includes(prevState.filters.search) ||
                    obj.license_plate.includes(prevState.filters.search)
            );
            if (prevState.filters._limit !== ALL_ITEMS_VALUE) {
                newScootersList = newScootersList.slice(
                    prevState.filters._limit * prevState.filters._page,
                    prevState.filters._limit
                );
            }

            return {
                tableData: newScootersList
            };
        });
    }
    getEntries() {
        HttpGET(SCOOTERS_ENDPOINT, this.state.filters).then(response => {
            const { scooters } = response.data.data;
            const TOTAL_ENTRIES = scooters.length;
            const TOTAL_PAGES = TOTAL_ENTRIES / this.state.filters._limit;
            this.setState(prevState => {
                return {
                    metaData: {
                        _totalEntries: TOTAL_ENTRIES,
                        _totalPages: TOTAL_PAGES
                    },
                    scooters,
                    tableHeaders:
                        prevState.tableHeaders.length === 1
                            ? [...prevState.tableHeaders, ...Object.keys(scooters[0])]
                            : prevState.tableHeaders,
                    tableData: scooters.slice(0, prevState.filters._limit)
                };
            });
        });
    }
    render() {
        const headers = this.state.tableHeaders.map(head => (
            <div>{head.replace(/_/g, ' ')}</div>
        ));
        const rows = this.state.tableData.map(scooter => (
            <ScooterTableRow key={scooter.id} scooter={scooter} />
        ));

        return (
            <div className="scooters">
                <div className="scooters--filters">
                    <p>Total scooters: {this.state.metaData._totalEntries}</p>
                    <TextInput
                        minLength={MIN_SEARCH_LENGTH}
                        onChange={this.updateSearch}
                    />
                    <SelectInput
                        name="perPage"
                        options={PAGE_RANGES}
                        onChange={this.updateParams}
                        label="Show: "
                    />
                </div>
                <div className="card">
                    <Table headers={headers} rows={rows} />
                </div>
            </div>
        );
    }
}

export default ScootersTable;
