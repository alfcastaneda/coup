import React from 'react';
import './Table.css';
const Table = props => (
    <table className="table">
        <thead>
            <tr>
                {props.headers.map((head, index) => (
                    <th key={`table-head-${index}`}>{head}</th>
                ))}
            </tr>
        </thead>
        <tbody>{props.rows}</tbody>
    </table>
);
export default Table;
