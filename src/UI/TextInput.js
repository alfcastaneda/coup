import React, { Component } from 'react';
import './TextInput.css';
class TextInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        };
        this.updateValue = this.updateValue.bind(this);
    }
    updateValue(event) {
        this.setState(
            {
                value: event.target.value
            },
            this.updateParent
        );
    }
    updateParent() {
        if (this.state.value.length >= this.props.minLength) {
            this.props.onChange(this.state.value);
        }
    }
    render() {
        return (
            <div className="input input--wrapper">
                <input
                    className="input--text"
                    value={this.state.value}
                    placeholder="Search scooter model ot plate"
                    onChange={this.updateValue}
                />
            </div>
        );
    }
}

export default TextInput;
