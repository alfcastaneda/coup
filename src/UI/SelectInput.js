import React from 'react';
import './Select.css';
const SelectInput = props => (
    <div className="selectable--wrapper">
        <label htmlFor={props.name}>{props.label}</label>
        <select className="selectable" name={props.name} onChange={props.onChange}>
            {props.options.map((opt, index) => (
                <option key={`${props.name}-${index}`} value={opt}>
                    {opt}
                </option>
            ))}
        </select>
    </div>
);

export default SelectInput;
